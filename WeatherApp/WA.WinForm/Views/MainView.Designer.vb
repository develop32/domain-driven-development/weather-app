﻿Imports MetroFramework.Forms
Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class MainView
        Inherits MetroForm

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.MainPanel = New MetroFramework.Controls.MetroPanel()
            Me.MetroStyleManager = New MetroFramework.Components.MetroStyleManager(Me.components)
            CType(Me.MetroStyleManager, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'MainPanel
            '
            Me.MainPanel.HorizontalScrollbarBarColor = True
            Me.MainPanel.HorizontalScrollbarHighlightOnWheel = False
            Me.MainPanel.HorizontalScrollbarSize = 10
            Me.MainPanel.Location = New System.Drawing.Point(23, 63)
            Me.MainPanel.Name = "MainPanel"
            Me.MainPanel.Size = New System.Drawing.Size(754, 514)
            Me.MainPanel.TabIndex = 0
            Me.MainPanel.VerticalScrollbarBarColor = True
            Me.MainPanel.VerticalScrollbarHighlightOnWheel = False
            Me.MainPanel.VerticalScrollbarSize = 10
            '
            'MetroStyleManager
            '
            Me.MetroStyleManager.Owner = Me
            '
            'MainView
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(800, 600)
            Me.Controls.Add(Me.MainPanel)
            Me.Font = New System.Drawing.Font("Meiryo UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
            Me.Margin = New System.Windows.Forms.Padding(4)
            Me.Name = "MainView"
            Me.Text = "MainView"
            CType(Me.MetroStyleManager, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents MainPanel As MetroFramework.Controls.MetroPanel
        Friend WithEvents MetroStyleManager As MetroFramework.Components.MetroStyleManager
    End Class

End Namespace