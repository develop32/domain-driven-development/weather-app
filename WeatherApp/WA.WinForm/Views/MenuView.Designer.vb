﻿Imports MetroFramework.Controls

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MenuView
    Inherits MetroUserControl

    'UserControl はコンポーネント一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ThemeSettingsTile = New MetroFramework.Controls.MetroTile()
        Me.WeatherTile = New MetroFramework.Controls.MetroTile()
        Me.AreaTile = New MetroFramework.Controls.MetroTile()
        Me.SuspendLayout()
        '
        'ThemeSettingsTile
        '
        Me.ThemeSettingsTile.ActiveControl = Nothing
        Me.ThemeSettingsTile.Location = New System.Drawing.Point(151, 147)
        Me.ThemeSettingsTile.Name = "ThemeSettingsTile"
        Me.ThemeSettingsTile.Size = New System.Drawing.Size(120, 120)
        Me.ThemeSettingsTile.TabIndex = 2
        Me.ThemeSettingsTile.Text = "テーマ設定"
        Me.ThemeSettingsTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ThemeSettingsTile.TileImage = Global.WA.WinForm.My.Resources.Resources.Settings
        Me.ThemeSettingsTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ThemeSettingsTile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.ThemeSettingsTile.UseSelectable = True
        Me.ThemeSettingsTile.UseStyleColors = True
        Me.ThemeSettingsTile.UseTileImage = True
        '
        'WeatherTile
        '
        Me.WeatherTile.ActiveControl = Nothing
        Me.WeatherTile.Location = New System.Drawing.Point(25, 21)
        Me.WeatherTile.Name = "WeatherTile"
        Me.WeatherTile.Size = New System.Drawing.Size(246, 120)
        Me.WeatherTile.TabIndex = 1
        Me.WeatherTile.Text = "天気情報"
        Me.WeatherTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.WeatherTile.TileImage = Global.WA.WinForm.My.Resources.Resources.iconmonstr_weather_1_64
        Me.WeatherTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.WeatherTile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.WeatherTile.UseSelectable = True
        Me.WeatherTile.UseStyleColors = True
        Me.WeatherTile.UseTileImage = True
        '
        'AreaTile
        '
        Me.AreaTile.ActiveControl = Nothing
        Me.AreaTile.Location = New System.Drawing.Point(25, 147)
        Me.AreaTile.Name = "AreaTile"
        Me.AreaTile.Size = New System.Drawing.Size(120, 120)
        Me.AreaTile.TabIndex = 0
        Me.AreaTile.Text = "地域マスタ"
        Me.AreaTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.AreaTile.TileImage = Global.WA.WinForm.My.Resources.Resources.iconmonstr_map_8_64
        Me.AreaTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AreaTile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.AreaTile.UseSelectable = True
        Me.AreaTile.UseStyleColors = True
        Me.AreaTile.UseTileImage = True
        '
        'MenuView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ThemeSettingsTile)
        Me.Controls.Add(Me.WeatherTile)
        Me.Controls.Add(Me.AreaTile)
        Me.Name = "MenuView"
        Me.Size = New System.Drawing.Size(500, 300)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents AreaTile As MetroFramework.Controls.MetroTile
    Friend WithEvents WeatherTile As MetroFramework.Controls.MetroTile
    Friend WithEvents ThemeSettingsTile As MetroTile
End Class
