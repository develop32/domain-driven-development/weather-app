﻿Imports MetroFramework.Controls

Namespace Views

    Public Class MainView
        Private Shared _instance As MainView

        Public Sub New()

            ' この呼び出しはデザイナーで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            _instance = Me
            StyleManager = Instance.MetroStyleManager
        End Sub

        Public Shared ReadOnly Property Instance As MainView
            Get
                If _instance Is Nothing Then
                    Return New MainView
                End If
                Return _instance
            End Get
        End Property

        Friend Sub ShowView(argViewName As String, isCreate As Boolean)
            Dim fullName As String = "WA.WinForm." & argViewName
            Dim type As Type = Type.GetType(fullName)
            Dim uc = TryCast(Activator.CreateInstance(type), MetroUserControl)
            If MainPanel.Controls.ContainsKey(argViewName) Then
                If isCreate Then
                    MainPanel.Controls.RemoveByKey(argViewName)
                Else
                    MainPanel.Controls(argViewName).BringToFront()
                    Return
                End If
            End If
            MainPanel.Controls.Add(uc)
            uc.Dock = DockStyle.Fill
            uc.Parent = _instance
            uc.BringToFront()
        End Sub

        Private Sub MainView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            ShowView(NameOf(MenuView), False)
        End Sub

    End Class

End Namespace